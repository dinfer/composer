import { documentReplaceEventHandler } from "./utils";
import { ConditionalListenerManager, TConditionFunction, IConditionalListener } from "./ConditionalListenerManager";

type TDragHandler = (dx: number, dy: number) => void;

let current: MouseEvent | null = null;
const pos = { x: 0, y: 0 };
const dragListeners = new ConditionalListenerManager<MouseEvent>();

function onMouseDown(e: MouseEvent) {
  current = e;
  pos.x = e.clientX;
  pos.y = e.clientY;
}

function onMouseUp(e: MouseEvent) {
  current = null;
}

function onMouseLeave(e: MouseEvent) {
  current = null;
}

function onMouseMove(e: MouseEvent) {
  if (!current) {
    return;
  }
  dragListeners.dispatch(current, (cb) => {
    const dx = e.clientX - pos.x;
    const dy = e.clientY - pos.y;
    if (dx != 0 || dy != 0) {
      e.preventDefault();
      cb(dx, dy);
      pos.x = e.clientX;
      pos.y = e.clientY;
    }
  });
}

documentReplaceEventHandler("mousedown", onMouseDown);
documentReplaceEventHandler("mouseup", onMouseUp);
documentReplaceEventHandler("mouseleave", onMouseLeave);
documentReplaceEventHandler("mousemove", onMouseMove);

export function useDrag(config: IConditionalListener<MouseEvent, TDragHandler>) {
  dragListeners.add(config);
}
