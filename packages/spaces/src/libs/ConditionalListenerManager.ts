export type TConditionFunction<T> = (v: T) => boolean;

export interface IConditionalListener<TValue, TListener> {
  condition: TConditionFunction<TValue>;
  listener: TListener;
}

export class ConditionalListenerManager<TValue, Listener = Function> {
  listeners: IConditionalListener<TValue, Listener>[] = [];

  constructor() {}

  add(config: IConditionalListener<TValue, Listener>) {
    if (this.listeners.find((v) => v.condition === config.condition || v.listener === config.listener)) {
      console.warn("duplicated register the same condition or listener");
    }
    this.listeners.push(config);
  }

  dispatch(event: TValue, cb: (listener: Listener) => void) {
    for (let i = 0; i < this.listeners.length; i++) {
      const entry = this.listeners[i];
      if (entry.condition(event)) {
        cb(entry.listener);
      }
    }
  }
}
