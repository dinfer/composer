export function documentReplaceEventHandler<K extends keyof DocumentEventMap>(
  event: K,
  listener: (this: Document, ev: DocumentEventMap[K]) => any,
  options?: boolean | AddEventListenerOptions,
) {
  document.removeEventListener(event, listener);
  document.addEventListener(event, listener, options);
}

export function windowReplaceEventHandler<K extends keyof WindowEventMap>(
  type: K,
  listener: (this: Window, ev: WindowEventMap[K]) => any,
  options?: boolean | EventListenerOptions,
) {
  window.removeEventListener(type, listener);
  window.addEventListener(type, listener, options);
}

export function ensure<T>(value: T | any): T {
  if (value === null || value === undefined) {
    throw new Error("should not be null or undefined");
  }
  return value;
}
