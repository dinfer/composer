import { TConditionFunction } from "./ConditionalListenerManager";

export function useWheel(cb: (e: WheelEvent) => void) {
  return {
    onWheel: (e: WheelEvent) => {
      cb(e);
    },
  };
}
