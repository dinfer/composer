import { onUnmounted } from "vue";

export function useResize(cb: (e?: UIEvent) => void) {
  window.addEventListener("resize", cb);
  onUnmounted(() => {
    window.removeEventListener("resize", cb);
  });
}
