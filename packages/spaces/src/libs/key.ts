import { onUnmounted } from "vue";
import { windowReplaceEventHandler } from "./utils";

interface IHandler {
  altKey: boolean;
  shiftKey: boolean;
  ctrlKey: boolean;
  key: string;
  listener: (e: KeyboardEvent) => void;
}

const registry: Array<IHandler> = [];

function exists(array: Array<IHandler>, item: IHandler) {
  return array.every(
    (v) =>
      v.listener === item.listener &&
      v.key === item.key &&
      v.shiftKey === item.shiftKey &&
      v.ctrlKey === item.ctrlKey &&
      v.altKey === v.altKey,
  );
}

function onKeyDown(e: KeyboardEvent) {
  console.log("keydown", e);
  for (let i = 0; i < registry.length; i++) {
    const v = registry[i];
    if (
      v.key.toLowerCase() === e.key.toLowerCase() &&
      v.shiftKey === e.shiftKey &&
      v.ctrlKey === e.ctrlKey &&
      v.altKey === e.altKey
    ) {
      v.listener(e);
    }
  }
}

function onKeyUp(e: KeyboardEvent) {
  for (let i = 0; i < registry.length; i++) {
    const v = registry[i];
    if (
      v.key.toLowerCase() === e.key.toLowerCase() &&
      v.shiftKey === e.shiftKey &&
      v.ctrlKey === e.ctrlKey &&
      v.altKey === e.altKey
    ) {
      v.listener(e);
    }
  }
}

windowReplaceEventHandler("keydown", onKeyDown);
windowReplaceEventHandler("keyup", onKeyUp);

export function useKey(config: { key: string; listener: (e?: KeyboardEvent) => void }) {
  const key = config.key
    .split("+")
    .map((v) => v.trim())
    .filter((v) => !!v)
    .pop();
  if (!key) {
    throw new Error("cannot get key");
  }

  const item = {
    altKey: config.key.includes("alt"),
    shiftKey: config.key.includes("shift"),
    ctrlKey: config.key.includes("ctrl"),
    key,
    listener: config.listener,
  };
  console.log("add key listener", item);

  if (!exists(registry, item)) {
    registry.push(item);
  }

  onUnmounted(() => {
    registry.splice(registry.indexOf(item), 1);
  });
}
