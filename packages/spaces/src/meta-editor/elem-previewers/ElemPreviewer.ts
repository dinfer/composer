import { h, defineComponent, Component } from "vue";
import { mappings, components } from './mappings';
import { Elem, EElemType } from "../elem/Elem";

const ElemPreviewer = defineComponent({
  components,
  props: {
    elem: {
      type: Object,
      required: true,
    },
  },
  render(p: { elem: Elem }) {
    if (p.elem.children.length > 0) {
      return h(
        "g",
        { id: p.elem.props.id },
        p.elem.children.map((v) => h(ElemPreviewer, { key: v.props.id, elem: v })),
      );
    }
    return h(mappings[p.elem.props.type], { elem: p.elem });
  },
});

export default ElemPreviewer;
