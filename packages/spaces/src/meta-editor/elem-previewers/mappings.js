import { EElemType } from "../elem/Elem";
import TextPreviewer from "./TextPreviewer";
import GroupPreviewer from "./GroupPreviewer";

export const mappings = {
  [EElemType.text]: TextPreviewer,
  [EElemType.group]: GroupPreviewer,
};

export const components = {
  TextPreviewer,
};
