import { h, defineComponent, Component } from "vue";
import { Elem, EElemType } from "../elem/Elem";

const ElemPreviewer = defineComponent({
  props: {
    elem: {
      type: Object,
      required: true,
    },
  },
  render(p: { elem: Elem }) {
    return h(
      "g",
      { id: p.elem.props.id },
      p.elem.children.map((v) => h(ElemPreviewer, { key: v.props.id, elem: v })),
    );
  },
});

export default ElemPreviewer;
