import CSS from "csstype";
import { TDict } from "../interfaces";
import _ from "lodash";

const magic = "composer";

let uid = 1;

export enum EElemType {
  group = "group",
  text = "text",
}

interface IElemPropsText {
  content: string;
}

interface IElemProps {
  id: number;
  type: EElemType;
  x: number;
  y: number;
  width: number;
  hegiht: number;
}

export class Elem {
  props: IElemProps;
  constructor(
    props: Partial<IElemProps & IElemPropsText> = {},
    readonly styles: CSS.Properties = {},
    readonly hooks: TDict = {},
    readonly children: Elem[] = [],
  ) {
    props.id = props.id || uid++;
    this.props = _.defaultsDeep(props, {
      type: EElemType.group,
      x: 0,
      y: 0,
      width: 100,
      height: 100,
    });
    switch (props.type) {
      case EElemType.text:
        this.props = _.defaultsDeep(this.props, {
          text: "文本",
        });
        break;
      case EElemType.group:
        break;
      default:
        throw new Error("cannot decide type");
    }
  }

  protected static _fromJSON(elem: any): Elem {
    if (elem.props.id > uid) {
      uid = elem.props.id++;
    }
    return new Elem(
      elem.props,
      elem.styles,
      elem.hooks,
      elem.children.map((v: any) => this._fromJSON(v)),
    );
  }

  static fromJSON(json: string): Elem {
    const data = JSON.parse(json);
    if (data.magic != magic) {
      throw new Error(`not valid magic. ${data.magic}`);
    }
    if (data.version != 0) {
      throw new Error("the version is not portable");
    }
    return this._fromJSON(data.elem);
  }

  static toJSON(elem: Elem): string {
    return JSON.stringify({
      magic,
      version: 0,
      elem,
    });
  }
}
