import { Elem, EElemType } from "./Elem";

export class ElemText extends Elem {
  constructor() {
    super();

    this.props.type = EElemType.text;
  }
}
