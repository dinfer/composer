export type TDict<V = any> = { [key: string]: V };
