export class LimitedArray<T = any> {
    protected data: T[] = [];
    constructor(readonly count: number) {
        this.count = Math.floor(count);
        if (this.count <= 0) {
            throw new Error(`count should be greater than zero`);
        }
    }

    get length(): number {
        return this.data.length;
    }

    /**
     * insert as the last
     * @param value value
     */
    push(value: T) {
        this.data.push(value);
        if (this.data.length > this.count) {
            this.data.shift();
        }
    }

    /**
     * return the last and remove it if exists
     */
    pop(): T | undefined {
        return this.data.pop();
    }

    /**
     * insert as the first
     * @param value value
     */
    unshift(value: T) {
        this.data.unshift(value);
        if (this.data.length > this.count) {
            this.data.pop();
        }
    }

    /**
     * return the first and remove it if exists
     */
    shift(): T | undefined {
        return this.data.shift();
    }

    get last(): T | undefined {
        return this.data[this.data.length - 1];
    }

    get first(): T | undefined {
        return this.data[0];
    }
}
