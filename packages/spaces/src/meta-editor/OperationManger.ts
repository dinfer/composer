import { LimitedArray } from "./LimitedArray";

interface IOperationManagerConfigs {
    limit: number;
}

export class NopOperation implements IOperation {
    constructor(public count = 0) {}
    async do(): Promise<void> {
        this.count++;
    }
    async undo(): Promise<void> {
        this.count--;
    }
}

export interface IOperation {
    do(): Promise<void>;
    undo(): Promise<void>;
}

export class OperationManager {
    opDo: LimitedArray<IOperation>;
    opRedo: LimitedArray<IOperation>;

    constructor(protected readonly configs: IOperationManagerConfigs) {
        this.opDo = new LimitedArray(configs.limit);
        this.opRedo = new LimitedArray(configs.limit);
    }

    async do(op: IOperation) {
        await op.do();
        this.opDo.push(op);
    }

    async undo() {
        const v = this.opDo.pop();
        if (!v) {
            throw new Error("cannot undo");
        }
        await v.undo();
        this.opRedo.push(v);
    }

    async redo() {
        const v = this.opRedo.pop();
        if (!v) {
            throw new Error("cannot redo");
        }
        await v.do();
        this.opDo.push(v);
    }

    get canUndo(): boolean {
        return this.opDo.length > 0;
    }

    get canRedo(): boolean {
        return this.opRedo.length > 0;
    }
}
