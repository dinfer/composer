import { Elem } from "../../src/meta-editor/elem/Elem";

describe("Elem", () => {
  it("serialize & deserialize", () => {
    const r = new Elem();
    r.children.push(new Elem());

    const s = Elem.fromJSON(Elem.toJSON(r));
    
    expect(s).toEqual(r);
    expect(s === r).toBeFalsy();
  });
});
