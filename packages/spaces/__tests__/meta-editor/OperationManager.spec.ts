import {
    OperationManager,
    IOperation,
    NopOperation,
} from "../../src/meta-editor/OperationManger";

describe("OperationManager", () => {
    let mgr: OperationManager;

    beforeEach(() => {
        mgr = new OperationManager({ limit: 1 });
    });

    it("do & redo", async () => {
        expect(mgr.canRedo).toEqual(false);
        expect(mgr.canUndo).toEqual(false);

        const op = new NopOperation(0);
        expect(op.count).toEqual(0);

        await mgr.do(op);
        expect(op.count).toEqual(1);
        expect(mgr.canRedo).toEqual(false);
        expect(mgr.canUndo).toEqual(true);

        await mgr.undo();
        expect(op.count).toEqual(0);
        expect(mgr.canRedo).toEqual(true);
        expect(mgr.canUndo).toEqual(false);

        await mgr.redo();
        expect(op.count).toEqual(1);
        expect(mgr.canRedo).toEqual(false);
        expect(mgr.canUndo).toEqual(true);
    });
});
