import { LimitedArray } from "../../src/meta-editor/LimitedArray";

describe("LimitedArray", () => {
    
    it("validate count", () => {
        expect(() => new LimitedArray(0)).toThrow();
        expect(() => new LimitedArray(-10)).toThrow();
    });

    it("fixed push & pop", () => {
        const v = new LimitedArray(1);
        expect(v.length).toEqual(0);
        expect(v.first).toBeUndefined();
        expect(v.last).toBeUndefined();

        v.push(10);
        expect(v.length).toEqual(1);
        expect(v.first).toEqual(10);
        expect(v.last).toEqual(10);

        v.push(20);
        expect(v.length).toEqual(1);
        expect(v.first).toEqual(20);
        expect(v.last).toEqual(20);

        v.pop();
        expect(v.length).toEqual(0);
        expect(v.first).toBeUndefined();
        expect(v.last).toBeUndefined();

        v.pop();
        expect(v.length).toEqual(0);
        expect(v.first).toBeUndefined();
        expect(v.last).toBeUndefined();
    });

    it("fixed shift & unshift", () => {
        const v = new LimitedArray(1);
        expect(v.length).toEqual(0);

        v.unshift(1);
        expect(v.length).toEqual(1);

        v.unshift(1);
        expect(v.length).toEqual(1);

        v.shift();
        expect(v.length).toEqual(0);

        v.shift();
        expect(v.length).toEqual(0);
    });
});
